
/**
 * Created by root on 16.12.15.
 */
public class Auto {

    public Motor m;
    public Rad [] r= new Rad[4];
    public String besitzer;



    public Auto(String b) {
        m= new Motor();
        for(int i =0 ; i< r.length; i++){
            r[i]=new Rad();
        }
        this.besitzer=b;
    }

    public void repariereAuto(Motor motor) {
        this.m=motor;
    }

    public void wechselRaeder(int anzahl) {

        for(int i =0; i<anzahl && i<r.length; i++) {
            r[i]=new Rad();
        }
    }

    public void verkaufeAuto(String neuerBesitzer) {
        this.besitzer = neuerBesitzer;

    }

    public void gebeAutoAus() {
        System.out.println("Auto: ");
        for(Rad k: r){
            k.druckeRad();
        }
        System.out.println("Besitzer: "+besitzer);
        System.out.println();
    }
}
