

import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.*;

public class CopyFile{

  protected StringReader in  = null;
  protected PrintWriter out = null;

  public void copy(String input, String output) throws IOException {

    try {

      in = new StringReader(new FileReader(input));
      out= new PrintWriter(new FileWriter(output));



      while(in.read() != -1 ) {

        out.write(Character.toUpperCase((char)in));
      }
  }catch (Exception e) {

      System.out.println(e);
    }
    finally {

      if(in!=null) {

        in.close();
      }
      if(out!=null) {

        out.close();
      }
    }
  }
}
