import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.*;

public class BufferedReaderExample {

	public static void main(String[] args) {

		BufferedReader br = null;
		PrintWriter out =null;
		int lines =0;
		int zeichen=0;
		int woerter=0;

		try {

			String sCurrentLine;

			//br = new BufferedReader(new FileReader("testing.txt"));
			//out = new PrintWriter(new FileWriter("ergebnis.txt"));
			br = new BufferedReader(new FileReader(args[0]));
			out = new PrintWriter(new FileWriter(args[1]));


			while ((sCurrentLine = br.readLine()) != null) {
				//System.out.println(sCurrentLine.toUpperCase());

								for(int i = 0; i<sCurrentLine.length(); i++) {
									zeichen ++;

									if(sCurrentLine.substring(i,i+1).equals(" ")) woerter++;

								}
				out.write(sCurrentLine.toUpperCase());

				out.write("\n");
				lines++;

			}
			woerter = woerter +lines;
			String s1 = "Anzahl der Zeilen "+lines+"\n";
			String s2 ="Anzahl der Zeichen " +zeichen+"\n";
			String s3 ="Anzahl der woerter " +woerter+"\n";
			out.write(s1, 0, s1.length() );
			out.write(s2, 0, s2.length() );
			out.write(s3, 0, s3.length() );

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)br.close();
				if(out != null)out.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

	}
}
