/**
 * Created by root on 16.12.15.
 */
public class MainApp {

    public static void main(String [] args) {
        Person p1 = new Person("rene");
        Person p2 = new Person("pascal");

        Vortrag v1 = new Vortrag("java");
        Vortrag v2= new Vortrag("python");
        Vortrag v3 = new Vortrag("whitespace");

        //person trägt sich für einen Vortragstermin ein
        p1.terminPlanen(v1);
        p1.terminPlanen(v2);
        p1.terminPlanen(v3);

        p2.terminPlanen(v3);
        p2.terminPlanen(v2);

        System.out.println("Vortrag:"+v1);
        v1.ausgeben();
        System.out.println();
        System.out.println("Vortrag:"+v2);
        v2.ausgeben();
        System.out.println();
        System.out.println("Vortrag: "+v3);
        v3.ausgeben();
        System.out.println();
        System.out.println("Person: "+p1);
        p1.ausgeben();
        System.out.println();
        System.out.println("Person: "+p2);
        p2.ausgeben();



    }
}
