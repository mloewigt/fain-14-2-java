/**
 * Created by root on 16.12.15.
 */
import java.util.*;

public class Person {
    private List < Vortrag >vortraege;
    public String name;

    public Person(String name) {
        this.vortraege=new ArrayList<>();
        this.name=name;
    }

    public void terminPlanen(Vortrag v) {

        if(!vortraege.contains(v)) {
            this.vortraege.add(v);
            v.einschreiben(this);
        }

    }

    public void ausgeben() {
        for( Vortrag k : vortraege) {
            System.out.println(k);
        }
    }

    public String toString() {
        return name;
    }
}
