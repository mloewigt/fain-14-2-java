/**
 * Created by root on 16.12.15.
 */
import java.util.*;

public class Vortrag {
    private List<Person> personen;
    private String name;

    public Vortrag(String n) {
        this.personen= new ArrayList<>();
        this.name=n;
    }

    public void einschreiben(Person p) {

        if(!personen.contains(p)) {
            this.personen.add(p);

            p.terminPlanen(this);
        }
    }
    public void ausgeben() {
        for(Person k : personen) {
            System.out.println(k);
        }
    }

    public String toString () {
        return name;
    }


}
