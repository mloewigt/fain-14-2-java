package puzzles;

import java.math.BigDecimal;

/**
 * Created by root on 01.12.15.
 */
public class OddEven {
    public static boolean isOdd(int i) {
        //return i%2!=0;
        return (i & 1)!=0;
    }
    public static void main(String [] args ) {
        if(isOdd(35))
            System.out.println("ist ungerade");
            else System.out.println("ist gerade");

        final long A=24*60*60*1000*1000L;
        final long B=24*60*60*1000;

        System.out.println(A);
        System.out.println(B);
        System.out.println(A/B);
        System.out.println(new BigDecimal("2.00").subtract(new BigDecimal("1.10")));
        System.out.println(12345+5432l);
        System.out.println(Long.toHexString(0x100000000L + 0xcafebabeL));
        System.out.println((int)(char)(byte)-10);



    }
}

