package de.steve72;

import de.steve72.tables.*;

public class BeispielTabelle {

	protected AbstractTableFactory tableFactory;


	// über den Konstruktor injizieren wir eine konkrete TabellenFabrik
	public BeispielTabelle(AbstractTableFactory atf) {

		this.tableFactory = atf;

	}

	// wir übergeben ein zweidimensionales Feld aus Zeichenketten
	public void showTable(String [][] data) {

		// wir lassen die injizierte Fabrik eine Tabelle erzeugen	
		Table table = this.tableFactory.createTable();

		// wir verarbeiten die beiden Dimensionen des Arrays
		
		// alle Zeilen verarbeiten
		for(String [] line: data) {

			Row row = this.tableFactory.createRow();

			table.addRow(row);

			// alle Spalten verarbeiten
			for(String content: line) {

				Cell cell = this.tableFactory.createCell(content);
				
				row.addCell(cell);
			}
		}

		// ...und die Tabelle ausgeben
		table.display();
	}
}
