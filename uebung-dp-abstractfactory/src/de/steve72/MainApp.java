package de.steve72;

import de.steve72.tables.*;

public class MainApp {

	public static void main(String [] args) {

		// die auszugebenen Testdaten
		String [][] data = new String[][] {

			{ "z1s1", "z1s2", "z1s3", "z1s4" },
			{ "z2s1", "z2s2", "z2s3", "z2s4" },
			{ "z3s1", "z3s2", "z3s3", "z3s4" }
		};


		// wir erzeugen uns eine Instanz der BeispielTabelle-Klasse (Director)
		BeispielTabelle bspHtml = new BeispielTabelle( new HtmlFactory() );
		BeispielTabelle bspText = new BeispielTabelle( new TextFactory() );
		//BeispielTabelle bspFile = new BeispielTabelle( new FileFactory() );

		// wir rufen die Ausgabe der Tabelle auf (unser Director-Objekt ruft createTable, createRow, createCell auf)
		bspHtml.showTable( data );
		bspText.showTable( data );
		//bspFile.showTable( data );	

	}
}
