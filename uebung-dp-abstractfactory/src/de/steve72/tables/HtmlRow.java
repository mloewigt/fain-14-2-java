package de.steve72.tables;

public class HtmlRow extends Row {

	@Override
	public void display() {

		// Html-Tag für den Beginn einer Tabellenzeile
		System.out.format("%8s%n", "<tr>");

		// für alle Zellen der Zeile 
		for(Cell cell: this.cells) {
		
			cell.display();		// ..schubsen wir die Html-Ausgabe an
		}

		// Html-Tg für das Ender einer Tabellenzeile
		System.out.format("%8s%n", "</tr>");
		
	}
}
