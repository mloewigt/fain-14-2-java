package de.steve72.tables;

public class HtmlCell extends Cell {

	public HtmlCell(String c) {

		super(c);
	}

	@Override
	public void display() {

		// Html-Tag für den Beginn der Zelle 
		System.out.format("%12s", "<td>");

		System.out.format("%s", this.content);

		// Html-Tag für den Ende der Zelle 
		System.out.format("%s%n", "</td>");
	}
}
