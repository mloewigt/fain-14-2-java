package de.steve72.tables;

public class HtmlFactory implements AbstractTableFactory {

	@Override
	public Table createTable() {

		return new HtmlTable();
	}

	@Override
	public Row createRow() {

		return new HtmlRow();
	}

	@Override
	public Cell createCell(String content) {

		return new HtmlCell(content);
	}
}

