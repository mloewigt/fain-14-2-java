package de.steve72.tables;


public interface AbstractTableFactory {

	public Table createTable();

	public Row createRow();

	public Cell createCell(String content);
}
