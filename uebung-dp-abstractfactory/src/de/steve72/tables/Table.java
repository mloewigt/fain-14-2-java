package de.steve72.tables;

import java.util.*;

/**
 * Die Klasse Table kapselt die Zeilen einer Tabelle und die Methode, um neue Zeilen zur Tabelle hinzuzufügen.
 *
 * Die "fertigen" Teile der Klasse Table sind für alle konkreten Produktklassen gleich. Einzig die Ausgabe
 * der Tabelle unterscheidet sich.
 *
 */

public abstract class Table {

	// eine Tabelle besteht aus Zeilen
	protected List<Row> rows;

	// der Konstruktor der Klasse erzeugt eine leere ArrayList
	public Table() {

		this.rows = new ArrayList<Row>();
	}

	// fügt eine neue Zeile zur Tabelle hinzu
	public void addRow(Row row) {

		this.rows.add(row);
	}

	// die abstrakte Methode muss in den konkreten Produktklassen (Text-, HTML-, FileTable)  überschrieben werden
	public abstract void display();
}
