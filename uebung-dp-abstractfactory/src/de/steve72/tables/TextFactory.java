package de.steve72.tables;

public class TextFactory implements AbstractTableFactory {

	@Override
	public Table createTable() {

		return new TextTable();
	}

	@Override
	public Row createRow() {

		return new TextRow();
	}

	@Override
	public Cell createCell(String content) {

		return new TextCell(content);
	}
}

