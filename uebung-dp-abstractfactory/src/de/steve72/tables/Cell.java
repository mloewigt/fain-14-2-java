package de.steve72.tables;

public abstract class Cell {

	// der Zelleninhalt
	protected String content;

	// der Konstruktor setzt den Inhalt der Zelle
	public Cell(String c) {

		this.content = c;
	}

	public abstract void display();
}
