package de.steve72.tables;

public class TextCell extends Cell {

	public TextCell(String c) {

		super(c);
	}

	@Override
	public void display() {

		System.out.format("%8s", this.content);
	}
}
