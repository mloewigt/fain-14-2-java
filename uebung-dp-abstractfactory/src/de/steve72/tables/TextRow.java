package de.steve72.tables;

public class TextRow extends Row {

	@Override
	public void display() {

		// für alle Zellen der Zeile
		for(Cell cell: this.cells) {

			cell.display();		// ..schubsen wir die Text-Ausgabe an
		}

		// Text-Tg für das Ender einer Tabellenzeile
		System.out.format("%n%s%n", "--------------------------------");

	}
}
