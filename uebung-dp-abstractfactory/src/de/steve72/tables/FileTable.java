package de.steve72.tables;

public class FileTable extends Table {

	@Override
	public void display() {

    Path file = Paths.get("/root/java/designpattern/filetable.txt");

		Path dir =file.getParent();
		try {
				// erzeugt *alle* übergeordneten Verzeichnisse
						Files.createDirectories(dir);

							// und die Datei
						Files.createFile(file);
		}
		catch(IOException e) {

				System.out.println(e);
		}
		
		System.out.format("%s%n", "--- Tabelle --------------------");

		// für jeder Zeile der Tabelle
		for(Row row: this.rows) {

			row.display();	// ...schubsen wir die Text-Ausgabe an
		}
	}
}
