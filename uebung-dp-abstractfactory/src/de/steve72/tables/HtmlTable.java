package de.steve72.tables;

public class HtmlTable extends Table {

	@Override
	public void display() {

		// Html-Tag für den Beginn der Tabelle
		System.out.format("%s%n", "<table>");


		// für jeder Zeile der Tabelle
		for(Row row: this.rows) {

			row.display();	// ...schubsen wir die Html-Ausgabe an
		}
		
		System.out.format("%s%n", "</table>");
		// Html-Tag für das Ende der Tabelle
	}
}
