package de.steve72.tables;

public class FileFactory implements AbstractTableFactory {

	@Override
	public Table createTable() {

		return new FileTable();
	}

	@Override
	public Row createRow() {

		return new FileRow();
	}

	@Override
	public Cell createCell(String content) {

		return new FileCell(content);
	}
}
