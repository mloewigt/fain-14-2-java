package de.steve72.tables;

import java.util.*;

public abstract class Row {

	// eine Zeile hat mehrere Zellen
	protected List<Cell> cells;

	// der Konstruktor erzeugt eine neue leere ArrayList
	public Row() {

		this.cells = new ArrayList<Cell>();
	}

	// die Methode fügt neue Zellen zu einer Zeile hinzu
	public void addCell(Cell cell) {

		this.cells.add(cell);
	}

	public abstract void display();
}
