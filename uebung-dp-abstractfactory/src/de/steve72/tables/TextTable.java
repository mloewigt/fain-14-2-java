package de.steve72.tables;

public class TextTable extends Table {

	@Override
	public void display() {

		System.out.format("%s%n", "--- Tabelle --------------------");

		// für jeder Zeile der Tabelle
		for(Row row: this.rows) {

			row.display();	// ...schubsen wir die Text-Ausgabe an
		}
	}
}
