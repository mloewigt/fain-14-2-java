/**
 * Created by root on 15.12.15.
 */
interface HasTail {
    int getTailLength();
}
