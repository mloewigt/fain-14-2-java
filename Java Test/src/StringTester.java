/**
 * Created by root on 15.12.15.
 */
public class StringTester {
    public static void main(String [] args) {
        String s1="Java";
        String s2="Java";

        Integer i1=10;
        Integer i2=10;

        StringBuilder sb1 = new StringBuilder();
        sb1.append("Ja").append("va");
        System.out.println(s1==s2);
        System.out.println(s1.equals(s2));
        System.out.println(sb1.toString()==s1);
        System.out.println(sb1.toString().equals(s1));

        System.out.println(i1==i2);

    }
}
