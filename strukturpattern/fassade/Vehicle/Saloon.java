package de.micha.vehicle;

import de.micha.engine.*;

public class Saloon extends AbstractCar {

	// Konstruktor
	public Saloon(Engine engine) {

		this(engine, Vehicle.Colour.UNPAINTED);
	}

	// Konstruktor
	public Saloon(Engine engine, Vehicle.Colour colour) {

		super(engine, colour);
	}
	@Override
	public int getPrice() {
					return 7000;
	}

	@Override
	public int getRent() {
					return 49;
	}

}
