package de.micha.vehicle;

public class SpoilerVehicle extends AbstractVehicleOption {

        public SpoilerVehicle(Vehicle vehicle){
                super(vehicle);
        }

        @Override
        public int getPrice() {

          return this.decoratedVehicle.getPrice() +400;
        }

        @Override
        public int getRent() {

          return this.decoratedVehicle.getRent() +7;
        }


}
