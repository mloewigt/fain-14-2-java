package de.micha.vehicle;

public class AirConditionedVehicle extends AbstractVehicleOption {

        public AirConditionedVehicle(Vehicle vehicle){
                super(vehicle);
        }

        @Override
        public int getPrice() {

          return this.decoratedVehicle.getPrice() +1200;
        }

        @Override
        public int getRent() {

          return this.decoratedVehicle.getRent() +12;
        }
      
}
