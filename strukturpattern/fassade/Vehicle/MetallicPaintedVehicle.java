package de.micha.vehicle;

public class MetallicPaintedVehicle extends AbstractVehicleOption {

        public MetallicPaintedVehicle(Vehicle vehicle){
                super(vehicle);
        }

        @Override
        public int getPrice() {

          return this.decoratedVehicle.getPrice() +500;
        }

        @Override
        public int getRent() {

          return this.decoratedVehicle.getRent() +4;
        }
        
}
