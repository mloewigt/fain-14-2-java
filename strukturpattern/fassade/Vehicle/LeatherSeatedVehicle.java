package de.micha.vehicle;

public class LeatherSeatedVehicle extends AbstractVehicleOption {

        public LeatherSeatedVehicle(Vehicle vehicle){
                super(vehicle);
        }

        @Override
        public int getPrice() {

          return this.decoratedVehicle.getPrice() +500;
        }

        @Override
        public int getRent() {
            return this.decoratedVehicle.getRent();

        }
      
}
