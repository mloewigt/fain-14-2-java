package de.micha.engine;

public interface Engine {

	// liefert den Hubraum
	public int getSize();

	// liefert true, falls es sich um einen Turbomotor handelt
	public boolean isTurbo();

  public void start();
  public void stop();
  public void increasePower();
  public void decreasePower();

	public void checkOil();
	public void checkEngine();
	public void writeDocuments();
	public void cleanEngine();
}
