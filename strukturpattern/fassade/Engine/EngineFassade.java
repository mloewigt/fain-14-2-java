package de.micha.fassade;

import de.micha.engine.*;

public class EngineFassade {
        public static void prepareForInstallment(Engine engine) {



              //und nun den Motor selbst vorbereiten
              engine.checkOil();
              engine.checkEngine();
              engine.writeDocuments();
              engine.cleanEngine();

        }
}
