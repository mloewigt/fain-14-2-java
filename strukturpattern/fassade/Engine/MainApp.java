

import de.micha.engine.*;
//import de.micha.engine.ElectroEngine;
import de.micha.bridge.*;
import de.micha.vehicle.*;
import de.micha.fassade.*;

public class MainApp {
    public static void main( String [] args) {
      /*
        Engine bridgeEngine = new ElectroEngine();
        StandardControls stdCtl = new StandardControls(bridgeEngine);

        stdCtl.ignitionOn();
        stdCtl.accelerate();
        stdCtl.brake();
        stdCtl.ignitionOff();


        SportControls sptCtl = new SportControls(bridgeEngine);

        sptCtl.ignitionOn();
        sptCtl.accelerate();
        sptCtl.brake();
        sptCtl.ignitionOff();

        VoiceControl vC = new VoiceControl(bridgeEngine);
        vC.accelerateLoud();
        vC.brakeLoud();
        */

        //Dekorierer-Pattern

        Vehicle decoCar= new Saloon(new StandardEngine(1600));
        System.out.println(decoCar);
        decoCar= new AirConditionedVehicle(decoCar);
        System.out.println(decoCar);
        decoCar = new LeatherSeatedVehicle(decoCar);
        System.out.println(decoCar);
        decoCar = new AlloyWheeledVehicle(decoCar);
        System.out.println(decoCar);
        decoCar = new SatNavVehicle(decoCar);
        System.out.println(decoCar);
        decoCar = new MetallicPaintedVehicle(decoCar);
        System.out.println(decoCar);
        decoCar = new SpoilerVehicle(decoCar);
        System.out.println(decoCar);

        Vehicle decoCoupe= new Coupe(new StandardEngine(1600));
        System.out.println(decoCoupe);
        decoCoupe= new AirConditionedVehicle(decoCoupe);
        System.out.println(decoCoupe);
        decoCoupe = new LeatherSeatedVehicle(decoCoupe);
        System.out.println(decoCoupe);
        decoCoupe = new AlloyWheeledVehicle(decoCoupe);
        System.out.println(decoCoupe);
        decoCoupe = new SatNavVehicle(decoCoupe);
        System.out.println(decoCoupe);
        decoCoupe = new MetallicPaintedVehicle(decoCoupe);
        System.out.println(decoCoupe);
        decoCoupe = new SpoilerVehicle(decoCoupe);
        System.out.println(decoCoupe);
        if(decoCoupe instanceof AbstractVehicleOption) {
                System.out.println("Der äußere Dekorierer wird entfernt");
                decoCoupe =((AbstractVehicleOption)decoCoupe).getDecoratedVehicle();
        }
        Vehicle fassadeVehicle = new Saloon(new StandardEngine(3000));
        VehicleFassade vf = new VehicleFassade();

        System.out.println("___________Fassade_________________");
        vf.prepareForSale(fassadeVehicle);
        System.out.println("___________EngineFassade_________________");
        Engine fassadeEngine = new StandardEngine(2000);
        EngineFassade ef = new EngineFassade();
        ef.prepareForInstallment(fassadeEngine);


    }



}
