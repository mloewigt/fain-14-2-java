package de.micha.vehicle;

import de.micha.engine.*;

public class Pickup extends AbstractVan {

	// Konstruktor
	public Pickup(Engine engine) {

		this(engine, Vehicle.Colour.UNPAINTED);
	}

	// Konstruktor
	public Pickup(Engine engine, Vehicle.Colour colour) {

		super(engine, colour);
	}

  @Override
  public int getPrice() {

    return 9000;
  }

  
}
