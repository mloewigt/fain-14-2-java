package de.micha.vehicle;

import de.micha.engine.*;

public abstract class AbstractVan extends AbstractVehicle {

	// Konstruktor
	public AbstractVan(Engine engine) {

		this(engine, Vehicle.Colour.UNPAINTED);
	}

	// Konstruktor
	public AbstractVan(Engine engine, Vehicle.Colour colour) {

		super(engine, colour);
	}
}
