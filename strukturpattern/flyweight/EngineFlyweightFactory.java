package de.micha.flyweight;

import de.micha.engine.*;
import java.util.*;

public class EngineFlyweightFactory {

        private Map <Integer, Engine> standardEnginePool;
        private Map <Integer, Engine> turboEnginePool;

        public EngineFlyweightFactory() {
                this.standardEnginePool = new HashMap<Integer, Engine>();
                this.turboEnginePool = new HashMap<Integer, Engine>();
        }
        public Engine getStandardEngine(int size) {
                Engine e = this.standardEnginePool.get(size);
                if(e == null) {
                      e = new StandardEngine(size);
                      this.standardEnginePool.put(size, e);
                }
                return e;
        }

        public Engine getTurboEngine(int size) {
                Engine e = this.turboEnginePool.get(size);
                if(e == null) {
                      e = new TurboEngine(size);
                      this.turboEnginePool.put(size, e);
                }
                return e;
        }
}
