package de.micha.flyweight;

public interface DiagnosticTool {
        public void runDiagnosis(Object o);
}
