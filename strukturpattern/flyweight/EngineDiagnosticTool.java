package de.micha.flyweight;

public class EngineDiagnosticTool implements DiagnosticTool {
        @Override
        public void runDiagnosis(Object o) {
                  System.out.println("Diagnose gestartet für "+ o);

                    try {
                          Thread.sleep(2000);
                          System.out.println("diganose abgeschlossen");

                    } catch (InterruptedException e) {
                            System.out.println("Diagnose wurde unterbrochen");
                    }
        }
}
