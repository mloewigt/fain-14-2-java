package de.micha.vehicle;

import de.micha.engine.*;

public class Coupe extends AbstractCar {

	// Konstruktor
	public Coupe(Engine engine) {

		this(engine, Vehicle.Colour.UNPAINTED);
	}

	// Konstruktor
	public Coupe(Engine engine, Vehicle.Colour colour) {

		super(engine, colour);
	}

  @Override
  public int getPrice() {

    return 7000;
  }


}
