package de.micha.engine;

import de.micha.flyweight.*;

public abstract class AbstractEngine implements Engine {

	// gemeinsame Eigenschaften aller Motoren
	private int size;
	private boolean turbo;
	private boolean running;
	protected int currentPower;
	protected int oillevel=100;

	public AbstractEngine(int size, boolean turbo) {

		this.size = size;
		this.turbo = turbo;
		this.running =false;
		this.currentPower=0;
	}

	@Override
	public int getSize() {

		return this.size;
	}

	@Override
	public boolean isTurbo() {

		return this.turbo;
	}

	@Override
	public String toString() {

		return String.format("%s (%d)", this.getClass().getSimpleName(), this.size);
	}
	@Override
	public void start (){
				this.running=true;
				System.out.println("Starting engine");

	}
	@Override
	public void stop(){
				this.running=false;
				System.out.println("Stopping engine");

	}



	@Override
	public void increasePower(){
		if (this.running && (this.currentPower<100)){
						this.currentPower+=1;

						System.out.println("Engine power increased to "+ this.currentPower);
		}
	}


	@Override
	public void decreasePower(){
		if (this.running && (this.currentPower>0)){
						this.currentPower-=1;

						System.out.println("Engine power decreased to "+ this.currentPower);
		}
	}
	@Override
	public void checkOil() {
		if(oillevel<150) {
					oillevel++;
		}
		System.out.println("Ölstand ist OK");

	}

	@Override
	public void checkEngine()  {
				System.out.println("Motor läuft");
	}

	@Override
	public void writeDocuments() {
						System.out.println("Protokoll fertig");
	}


	@Override
	public void cleanEngine() {
					System.out.println("und sauber ist der Motor auch");
	}

  @Override
  public void diagnose(DiagnosticTool tool) {
        tool.runDiagnosis(this);
  }

}
