package de.micha.vehicle;

import de.micha.engine.*;

public interface Vehicle extends Cloneable {

	// Stichwort: nested class
	// Farben dann später verwendbar z.B.  als Vehicle.Colour.RED
	public enum Colour { UNPAINTED, BLUE, BLACK, GREEN, RED, SILVER, WHITE, YELLOW };

	public Engine getEngine();
	public Vehicle.Colour getColour();
	public void paint(Vehicle.Colour colour);


	// die Methode clone des Interface Cloneable (damit darf für alle Vehicle die Methode clone aufgerufen werden)
	public Object clone();

  // Methoden zum Üben des Bridge-Patterns
  public void move(int km);

  // Methode(n) für das Dekorierer-Muster
  public int getPrice();
  public int getDailyRate();  // Tagessatz für den Verleih

  // Methoden für das Fassade-Pattern
  public void cleanInterior();
  public void cleanExterior();
  public void polishWindows();
  public void testDrive();
}
