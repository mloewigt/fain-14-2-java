
import de.micha.engine.*;


import de.micha.vehicle.*;

import de.micha.flyweight.*;

public class MainApp {
    public static void main( String [] args) {

        Vehicle fassadeVehicle = new Saloon(new StandardEngine(3000));
        VehicleFassade vf = new VehicleFassade();

        System.out.println("___________Fassade_________________");
        vf.prepareForSale(fassadeVehicle);
        System.out.println("___________EngineFassade_________________");
        Engine fassadeEngine = new StandardEngine(2000);
        EngineFassade ef = new EngineFassade();
        ef.prepareForInstallment(fassadeEngine);

        EngineFlyweightFactory eff = new EngineFlyweightFactory();
        Vehicle v01 = new Saloon(eef.getStandardEngine(1200));
        Vehicle v02 =new Pickup(eff.getTurboEngine(3000));
        Vehicle v03=new Coupe(eff.getStandardEngine(1200));

        //mal eine Kontrollausgabe
        System.out.format("Motor von v01: %d, Motor von v02 %d", v01.getEngine().hashCode(), v03.getEngine().hashCode());
    }



}
