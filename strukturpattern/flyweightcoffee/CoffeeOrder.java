package de.micha.flyweightcoffee;

public interface CoffeeOrder {

      public void serveCoffee(CoffeeOrderContext context);
      
}
