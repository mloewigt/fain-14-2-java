package de.micha.vehicle;

import de.micha.engine.*;
import de.micha.component.*;

public abstract class AbstractVehicle implements Vehicle {

  // gemeinsame Eigenschaften der Fahrzeuge
  private Engine engine;
  private Vehicle.Colour colour;
  private Window window;


  protected int milage;

  // Konstruktor

  public AbstractVehicle() {

    this(new StandardEngine(1300), Vehicle.Colour.UNPAINTED); // klassenintern wird der andere konstruktor aufgerufen
  }

  public AbstractVehicle(Engine engine) {

    this(engine, Vehicle.Colour.UNPAINTED); // klassenintern wird der andere konstruktor aufgerufen
  }

  // Konstruktor
  public AbstractVehicle(Engine engine, Vehicle.Colour colour) {

    this.engine = engine;
    this.colour = colour;
  }

  @Override
  public Engine getEngine() {

    return this.engine;
  }

  @Override
  public Vehicle.Colour getColour() {

    return this.colour;
  }

  @Override
  public void paint(Vehicle.Colour colour) {

    this.colour = colour;
  }

  public void addWindow(Window window) {

    this.window = window;
  }

  @Override
  public String toString() {

    StringBuilder sb = new StringBuilder();

    //sb.append(String.format("%s (%s, %s), Preis %d", this.getClass().getSimpleName(), this.engine, this.colour, this.getPrice()));
    sb.append(String.format("%s (%s, %s), Miete %d", this.getClass().getSimpleName(), this.engine, this.colour, this.getRent()));
    if(this.window != null) {

      sb.append(String.format(" %s", this.window));
    }

    return sb.toString();
  }

  @Override
  public Object clone() {

    Object obj = null;

    try {

      // die Methode clone() der BasisKlasse wird aufgerufen
      obj = super.clone();
    }
    catch(CloneNotSupportedException e) {

      System.err.println(e);
    }

    return obj;
  }

  @Override
  public void move(int km ) {
        if(km>0) {
              this.milage +=km;
              System.out.println("neuer Kilometerstand: "+this.milage);
        }
  }
  @Override
  public void cleanInterior(){
    System.out.println("Reinige den Innenraum ..");
  }

  @Override
  public void cleanExterior(){
    System.out.println("Reinige das Äußere des Fahrzeugs..");
  }

  @Override
  public void polishWindows(){
    System.out.println("Fenster sind sauber");
  }
  @Override
  public void testDrive(){
    this.move(2);
    System.out.println("Testfahrt ausgeführt");
  }




}
