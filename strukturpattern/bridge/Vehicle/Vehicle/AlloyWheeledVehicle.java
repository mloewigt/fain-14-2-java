package de.micha.vehicle;

public class AlloyWheeledVehicle extends AbstractVehicleOption {

        public AlloyWheeledVehicle(Vehicle vehicle){
                super(vehicle);
        }

        @Override
        public int getPrice() {

          return this.decoratedVehicle.getPrice() +250;
        }
        @Override
        public int getRent() {

          return this.decoratedVehicle.getRent() +3;
        }
        
}
