package de.micha.vehicle;

public abstract class AbstractVehicleOption extends AbstractVehicle {

        protected Vehicle decoratedVehicle;

        public AbstractVehicleOption(Vehicle vehicle) {
                super(vehicle.getEngine());
                this.decoratedVehicle=vehicle;
        }
                
        public Vehicle getDecoratedVehicle() {
          return this.decoratedVehicle;
        }
}
