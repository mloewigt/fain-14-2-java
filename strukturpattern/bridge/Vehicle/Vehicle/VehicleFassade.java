package de.micha.fassade;

import de.steve72.vehicle.*;

public class VehicleFassade {
        public void prepareForSale(Vehicle vehicle) {

              //den Papierkram erledigen
              Registration reg = new Registration (vehicle);

              reg.allocateLicencePlate();
              reg.getVehicleDocuments();

              //und nun das Fahrzeug selbst vorbereiten
              vehicle.testDrive();
              vehicle.cleanInterior();
              vehicle.cleanExterior();
              vehicle.polishWindows();
        }
}
