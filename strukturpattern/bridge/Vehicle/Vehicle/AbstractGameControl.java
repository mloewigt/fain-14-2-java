package de.micha.bridge;

import de.micha.vehicle.*;

public abstract class AbstractGameControl {


              private Vehicle vehicle;


              public AbstractGameControl (Vehicle v) {
                        this.vehicle=v;
              }

              public void turn() {

                this.vehicle.move(1);
              }
}
