package de.micha.vehicle;

public class SatNavVehicle extends AbstractVehicleOption {

        public SatNavVehicle(Vehicle vehicle){
                super(vehicle);
        }

        @Override
        public int getPrice() {

          return this.decoratedVehicle.getPrice() +300;
        }
        @Override
        public int getRent() {

          return this.decoratedVehicle.getRent() +5;
        }
      
}
