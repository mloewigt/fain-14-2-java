package de.micha.vehicle;

import de.micha.engine.*;

public abstract class AbstractCar extends AbstractVehicle {

	// Konstruktor
	public AbstractCar(Engine engine) {

		this(engine, Vehicle.Colour.UNPAINTED);
	}

	// Konstruktor
	public AbstractCar(Engine engine, Vehicle.Colour colour) {

		super(engine, colour);
	}
}
