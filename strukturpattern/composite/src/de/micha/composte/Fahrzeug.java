package de.micha.composite;

import java.io.*;
import java.io.File.*;
import java.io.FileWriter.*;

public class Fahrzeug implements Verleih {

        //Attribute
        //private Car fahrzeug;
        private String description;



        //Konstruktoren
        public Fahrzeug () {
            this.description="Standardvehikel";

        }

        public Fahrzeug (String bezeichnung) {
            this.description=bezeichnung;

        }




        //Methoden
        @Override
        public void toConsole() {
              System.out.format("Fahrzeug: %s am Standort %s %n", this.getClass().getSimpleName(), this.description);
        }
        @Override
        public void toFile(){

              try {
                  File out = new File("fahrzeuge.txt");
                  FileWriter fw = new FileWriter(out);
                  BufferedWriter bw = new BufferedWriter(fw);
                  String content= "Fahrzeug: %s am Standort %s %n" +" " + this.getClass().getSimpleName()+" "+ this.description;

                  bw.write(content);
                  bw.close();
              } catch (IOException e) {
                      System.err.println(e);
              }


        }



}
