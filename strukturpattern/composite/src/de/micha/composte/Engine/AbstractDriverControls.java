package de.micha.bridge;

import de.micha.engine.*;

public class AbstractDriverControls {

        private Enging engine;

        public AbstractDriverControl(Engine engine) {
                  this.engine=engine;
        }

        public void ignitionOn() {

                  this.engine.start();
        }

        public void ignitionOff(){
                  this.engine.stop();
        }

        public void accelerate() {
                  this.engine.increasePower();
        }
        public void brake() {
                  this.engine.decreasePower();
        }
}
