package de.Micha.*;

import de.micha.engine.*;
import de.micha.brigde.*;

public class MainApp {
    public static void main( String [] args) {
        Engine bridgeEngine = new StandardEngine(1800);
        StandardControls stdCtl = new StandardControls(brigeEngine);

        stdCtl.ignitionOn();
        stdCtl.accelerate();
        stdCtl.brake();
        stdCtl.ignitionOff();


        SportControls sptCtl = new SportControls(brigdeEngine);

        sptCtl.ignitionOn();
        sptCtl.accelerate();
        sptCtl.brake();
        sptCtl.ignitionOff();
    }



}
