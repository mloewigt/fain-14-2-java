package de.micha.composite;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.io.File.*;
import java.io.FileWriter.*;

public class Standort implements Verleih{


        //Attribute
        private String standortBez;

        public List <Verleih> inventar=null;


        //Konstruktoren
        public Standort (String name) {
          this.standortBez=name;

          this.inventar=new ArrayList<>();

        }


        //Methoden
        @Override
        public void toConsole() {
              System.out.format("Dieser Standort: %s besteht aus: %n", this.getClass().getSimpleName());

              for(Verleih v : this.inventar) {
                    v.toConsole();
              }
        }
        public void addInventar(Verleih v) {
            this.inventar.add(v);

        }

        @Override
        public void toFile(){

              try {
                  File out = new File("standorte.txt");
                  FileWriter fw = new FileWriter(out);
                  BufferedWriter bw = new BufferedWriter(fw);
                  String content= "Dieser Standort: %s besteht aus: %n"+ this.getClass().getSimpleName();

                  bw.write(content);
                  bw.close();
              } catch (IOException e) {
                      System.err.println(e);
              }


        }

}
