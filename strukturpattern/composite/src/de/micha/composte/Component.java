package de.micha.composite;

import java.util.*;

public class Component extends Item {

        private List<Item> items;

        public Component(String s) {
            super(s,0);
            this.items = new ArrayList<Item>();
        }

        public void addItem(Item item) {
              this.items.add(item);
        }

        public void removeItem(Item item) {
              this.items.remove(item);
        }

        public Item[] getItems(){
                return this.items.toArray(new Item[this.items.size()]);
        }

        public int getCost() {
              int total = 0;
              for(Item item: this.items) {
                    total +=item.getCost();
              }
              return total;
        }
        public String toString() {


              String teile=this.getDescription()+" besteht aus und kostet " +this.getCost()+"\n";
              teile +=" ";
              for(Item item: this.items) {

                    //teile+=item.getDescription()+" "+ item.getCost()+" \n";
                    teile+=item.toString()+" \n ";
              }
              return String.format(teile);
              //return String.format("Die Komponente besteht aus\n", teile);
        }
}
