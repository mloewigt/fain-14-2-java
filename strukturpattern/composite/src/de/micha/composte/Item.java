package de.micha.composite;

public abstract class Item {
      private String description;
      private int cost;


      public Item (String s, int c) {
              this.description = s;
              this.cost =c;
      }

      public String getDescription() {
              return this.description;
      }
      public int getCost(){
              return this.cost;
      }
      public abstract void addItem(Item item);
      public abstract void removeItem(Item item);
      public abstract Item [] getItems();

      @Override
      public String toString(){
              return String.format("%s :(%d Euro)", this.description, this.getCost());
      }

}
