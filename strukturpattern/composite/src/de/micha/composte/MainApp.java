

import de.micha.composite.*;

public class MainApp {

      public static void main(String [] args) {
            Item nut = new Part("Nut", 5);
            Item bolt = new Part("Bolt", 9);
            Item panel = new Part("Panel", 35);

            Item compA = new Component("Komponente A");
            compA.addItem(nut);
            compA.addItem(bolt);
            compA.addItem(panel);
            Item compB = new Component("Komponente b");
            compB.addItem(compA);
            compB.addItem(nut);
            Item compC = new Component("Komp C");
            compC.addItem(compA);
            compC.addItem(compB);
            compC.addItem(bolt);

            /*
            System.out.println(nut);
            System.out.println(bolt);
            System.out.println(panel);
            System.out.println(compA);
            System.out.println(compB);
            */
            System.out.println(compC);
      }
}
