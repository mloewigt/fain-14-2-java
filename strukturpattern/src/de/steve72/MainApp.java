package de.steve72.adapter;

import de.steve72.engine.*;
import java.util.*;
import de.steve72.adapter.*;
import de.steve72.truck.*;


public class MainApp {

      public static void main (String [] args ) {
              List<Engine> engines = new ArrayList<Engine>();

              engines.add(new StandardEngine(1300));
              engines.add(new StandardEngine(1600));
              engines.add(new StandardEngine(1800));
              engines.add(new StandardEngine(2000));

              engines.add(new TurboEngine(2000));
              engines.add(new TurboEngine(2200));
              engines.add(new TurboEngine(2600));



              engines.add(new AdapterEngine(new GreenEngine(1100)));
              engines.add(new AdapterEngineAlternativ(new GreenEngine(900)));
              engines.add(new AdapterEngineAlternativ(new GreenEngine(600)));



              for(Engine e: engines) {
                    System.out.println(e);
              }
              TruckAdapter ta = new TruckAdapter(new DumpTruck(2000, 30));
              System.out.println(ta);
              ta = new TruckAdapter(new SemiTrailerTruck(40000,400));
              System.out.println(ta);

              TruckAdapterAlternative taa = new TruckAdapterAlternative(new SemiTrailerTruck(10000, 40));
              System.out.println(taa);
              System.out.println("Aussehen Truck: "+taa.getColour());

      }
}
