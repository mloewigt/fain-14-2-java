package de.steve72.adapter;

import de.steve72.engine.*;

public class AdapterEngineAlternativ extends AbstractEngine {
      // wir schaffen die Möglichkeit, das zu adaptierende Objekt zu referenzieren

      private GreenEngine greenEngine;

      public AdapterEngineAlternativ(GreenEngine ge) {
              super(0, false);
              this.greenEngine = ge;
              //wir speichern die Referenz auf das zu adaptierende Objekt
      }

      @Override
      public int getSize() {
            return this.greenEngine.getEngineSize();
      }
      @Override
      public boolean isTurbo() {
            return false;
      }

      @Override
      public String toString () {
              return String.format("%s (%d)", this.greenEngine.getClass().getSimpleName(), this.getSize());
      }



}
