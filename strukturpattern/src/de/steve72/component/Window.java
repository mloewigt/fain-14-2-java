package de.steve72.component;

public class Window {

	@Override
	public String toString() {
		
		return String.format("[%s (glasklar wie unsichtbar)]", this.getClass().getSimpleName());
	}
}
