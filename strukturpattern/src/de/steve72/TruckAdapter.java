package de.steve72.adapter;

import de.steve72.vehicle.*;
import de.steve72.engine.*;
import de.steve72.component.*;
import de.steve72.adapter.*;
import de.steve72.truck.*;

public class TruckAdapter extends AbstractVehicle {

      public TruckAdapter(Truck truck) {
            super(truck.getTruckEngine());

      }

      public TruckAdapter(Truck truck, Vehicle.Colour colour) {
            super(truck.getTruckEngine(), colour);
      }

}
