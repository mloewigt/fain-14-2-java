package de.steve72.vehicle;

import de.steve72.engine.*;
import de.steve72.component.*;

public abstract class AbstractVehicle implements Vehicle {

	// gemeinsame Eigenschaften der Fahrzeuge
	private Engine engine;
	private Vehicle.Colour colour;
	private Window window;

	// Konstruktor
	public AbstractVehicle(Engine engine) {

		this(engine, Vehicle.Colour.UNPAINTED);	// klassenintern wird der andere konstruktor aufgerufen
	}

	// Konstruktor
	public AbstractVehicle(Engine engine, Vehicle.Colour colour) {

		this.engine = engine;
		this.colour = colour;
	}

	@Override
	public Engine getEngine() {

		return this.engine;
	}

	@Override
	public Vehicle.Colour getColour() {

		return this.colour;
	}

	@Override
	public void paint(Vehicle.Colour colour) {

		this.colour = colour;
	}

	public void addWindow(Window window) {

		this.window = window;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append(String.format("%s (%s, %s)", this.getClass().getSimpleName(), this.engine, this.colour));

		if(this.window != null) {

			sb.append(String.format(" %s", this.window));
		}

		return sb.toString();
	}
}
