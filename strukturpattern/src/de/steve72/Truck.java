package de.steve72.truck;

import de.steve72.engine.*;


public class Truck {

    private int engineSize;
    private int load;
    private Engine myEngine;

    public Truck() {
        this.engineSize = 0;
        this.load = 0;
        this.myEngine=new TurboEngine(engineSize);
    }

    public Truck( int size, int load) {
        this.engineSize=size;
        this.load=load;
        this.myEngine=new TurboEngine(engineSize);
    }

    public Engine getTruckEngine() {
          return this.myEngine;
    }

    public int getPayload() {
          return this.load;
    }


}
