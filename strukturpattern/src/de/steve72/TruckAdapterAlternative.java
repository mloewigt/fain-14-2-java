package de.steve72.adapter;

import de.steve72.truck.*;
import de.steve72.vehicle.*;
import de.steve72.component.*;
import de.steve72.engine.*;

public class TruckAdapterAlternative extends AbstractVehicle {


      private Truck truck;

      private Window w = new Window();

      public TruckAdapterAlternative(Truck truck) {
              super(truck.getTruckEngine() );
              this.truck = truck;
      }

      @Override
      public Engine getEngine() {

        return this.truck.getTruckEngine();
      }


    


      @Override
      public void paint(Vehicle.Colour colour) {

        this.paint(colour);
      }

      public void addWindow(Window window) {

        this.addWindow(w);
      }

      @Override
      public String toString() {

        StringBuilder sb = new StringBuilder();

        sb.append(String.format("%s (%s, %s)", truck.getClass().getSimpleName(), this.getEngine(), this.getColour()));

        if(w != null) {

          sb.append(String.format(" %s", w));
        }

        return sb.toString();
      }


}
