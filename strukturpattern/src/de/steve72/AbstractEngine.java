package de.steve72.engine;

public abstract class AbstractEngine implements Engine {

	// gemeinsame Eigenschaften aller Motoren
	private int size;
	private boolean turbo;

	public AbstractEngine(int size, boolean turbo) {

		this.size = size;
		this.turbo = turbo;
	}

	@Override
	public int getSize() {

		return this.size;
	}

	@Override
	public boolean isTurbo() {

		return this.turbo;
	}

	@Override
	public String toString() {

		return String.format("%s (%d)", this.getClass().getSimpleName(), this.size);
	}
}
