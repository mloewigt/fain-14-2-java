package de.steve72.vehicle;

import de.steve72.engine.*;

public interface Vehicle {

	// Stichwort: nested class
	// Farben dann später verwendbar z.B.  als Vehicle.Colour.RED
	public enum Colour { UNPAINTED, BLUE, BLACK, GREEN, RED, SILVER, WHITE, YELLOW };

	public Engine getEngine();
	public Vehicle.Colour getColour();
	public void paint(Vehicle.Colour colour);

}
