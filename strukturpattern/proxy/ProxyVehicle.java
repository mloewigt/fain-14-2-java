package de.micha.proxy;


import de.micha.vehicle.*;
import de.micha.engine.*;


public class ProxyVehicle implements Vehicle{
      private Vehicle vehicle;


      private int gefahreneKilometer =0;
      private int maxKilometer =0;

      //der Konstruktor
      public ProxyVehicle(int size, boolean isTurbo) {
              //wir verleihen ausschließlich Limos
              if(isTurbo) {
                    this.vehicle = new Saloon(new TurboEngine(size));
              } else {
                    this.vehicle = new Saloon( new StandardEngine(size));
              }
      }

      //wir überschreiben die Methoden des Interface
      @Override
      public void move (int km) {
              if(this.gefahreneKilometer + km > this.maxKilometer) {
                    System.out.println("sie müssen neuen Km-Stand erwerben");
              } else {
                    this.gefahreneKilometer += km;
                    this.vehicle.move(km);
              }
      }

      public void guthabenAufladen(int km) {
            if(km>0) {
                  this.maxKilometer += km;
            }
      }
      @Override
      public int getPrice() {
            return this.vehicle.getPrice();
      }

      @Override
      public int getDailyRate() {
            return this.vehicle.getDailyRate();
      }
      @Override
      public void cleanInterior() {
            this.vehicle.cleanInterior();
      }
      @Override
      public void cleanExterior() {
            this.vehicle.cleanExterior();
      }
      @Override
      public void polishWindows() {
            this.vehicle.polishWindows();
      }
      @Override
      public void testDrive() {
            this.testDrive();
      }
      @Override
      public int cleanInterior() {
            return this.vehicle.cleanInterior();
      }


}
