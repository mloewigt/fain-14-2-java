/**
 * Created by root on 16.12.15.
 */
public class Datei extends FSO {


    public Datei(String n, int s) {
        this.name=n;
        this.size=s;
    }

    public void ausgabe() {
        System.out.format("Datei: %s mit einer Größe von %d ", this.name, this.size);
    }

}
