/**
 * Created by root on 16.12.15.
 */
public class MainApp {
    public static void main(String [] args) {
        Verzeichnis v1 = new Verzeichnis("folder 1");
        Verzeichnis v2 = new Verzeichnis("folder 2");
        Verzeichnis v3 = new Verzeichnis("folder 3");
        Verzeichnis v4 = new Verzeichnis("folder 4");
        Verzeichnis v5 = new Verzeichnis("folder 5");

        int a = 10;
        Datei d1 = new Datei("datei1", a);
        Datei d2 = new Datei("datei2", a*a);
        Datei d3 = new Datei("datei3", a*a+a);

        v1.addElements(v1);
        v1.addElements(d1);

        v2.addElements(v1);

        v2.ausgebenFolder();
        v1.ausgebenFolder();
    }
}
