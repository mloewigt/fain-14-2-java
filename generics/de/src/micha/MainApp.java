package de.src.micha;

public class MainApp {
  public static void main(String [] args) {
    Box b = new Box();
    b.add(new String("Hallo"));
    System.out.format("Der Inhalt der Box ist: %s %n", b.getContent());

    Box b2 = new Box();
    b2.add(new Car());
    System.out.format("Der Inhalt der Box ist: %s %n", b2.getContent());

    ((Car)b2.getContent()).move(100);
    System.out.format("erneuter Aufruf: %s %n", b2.getContent());

    BoxGeneric <Car>bg = new BoxGeneric<Car>();
    bg.add(new Car());
    bg.getContent().move(500);

    System.out.format("Mit den Generics: %s %n", bg.getContent());

    BoxGeneric <Book> book = new BoxGeneric<Book>();
    book.add(new Book());
    book.getContent().lesen();

    Fuhrpark<Lkw> fp = new Fuhrpark<Lkw>();
    fp.add( new Lkw(40) );
    fp.ausprobieren();
    Fuhrpark <Car> taxi = new Fuhrpark<Car>();
    taxi.add(new Car());
    taxi.ausprobieren();

    Pair<Integer, String> p1 = new Pair<Integer, String>(1, "Birne");
    Pair<Integer, String> p2 = new Pair<Integer, String>(1, "Apfel");

    Pair<String, Integer> p3 = new Pair<String, Integer>("Birne", 1);
    if(Tool.<Integer, String>vergleiche(p1, p2))
      System.out.println("... sind gleich");
    else
        System.out.println("... sind ungleich");

    Pair<Integer, String> p4 = new Pair<>(4, "Banane");
    if(Tool.vergleiche(p4, p4))
      System.out.println("... sind gleich");
    else
        System.out.println("... sind ungleich");
  }


}
