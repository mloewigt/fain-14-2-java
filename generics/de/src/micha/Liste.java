package de.src.micha;

public class Liste <E> {
  protected E element;

  public Liste (E ele) {
    this.element=ele;
  }

  public boolean vergleiche( Liste <E> l1, Liste <E> l2) {
    return l1.getElement().equals(l2.getElement());
  }

  public E getElement() {
    return this.element;
  }

}
