package de.src.micha;


public abstract class AVehicle implements Vehicle {
  private int mileage=0;

  @Override
  public String toString() {
    return String.format("Ich bin ein Objekt der Klasse %s und habe %d km auf dem Tacho ", this.getClass().getSimpleName(), this.mileage);
  }


    @Override
    public void move(int km){
        this.mileage+=km;
    }

}
