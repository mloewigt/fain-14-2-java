package de.src.micha;

public class Tool {

  public static <K,V> boolean vergleiche(Pair <K,V> p1, Pair<K,V> p2) {
    // zwei Wertepaare sollen gleich sein, wenn deren Schlüssel und deren Werte gleich sind
    //equals verwenden und nicht ==

    return p1.getKey().equals(p2.getKey()) && p1.getValue().equals(p2.getValue());
  }
}
