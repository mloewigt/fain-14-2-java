package de.src.micha;


public class Car implements Vehicle {
  // oder extends AVehicle

    private int mileage =0;
    @Override
    public void move(int km) {
      this.mileage+=km;
    }

    @Override
    public String toString() {
      return String.format("Ich bin ein %s unf habe %d auf dem Tacho", this.getClass().getSimpleName(), this.mileage);

    }

}
