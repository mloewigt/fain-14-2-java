package de.src.micha;

public class Fuhrpark<T extends Vehicle> {
  private T slots;
  // der Konstruktor
  public Fuhrpark() {

  }
  public void add(T obj){

    this.slots=obj;
  }
  public void ausprobieren() {

      slots.move(1);

      /*
      * funktioniert nur deshalb, weil durch den upper bound "extends Vehicle"
      * Objekte der Klassenhierarchie für die generische Klasse verwendet
      * müssen, Programmieren
      */
    }
  }
