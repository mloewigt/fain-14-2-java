package de.src.micha;


public interface Vehicle {

  public void move(int km);
}
