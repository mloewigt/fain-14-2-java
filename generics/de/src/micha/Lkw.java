package de.src.micha;

public class Lkw extends AVehicle {
  private int capacity;

  public Lkw(int t){
    this.capacity=t;
  }

  @Override
  public String toString() {
    return String.format("%s mit einer max. Zuladung von %d t", super.toString(), this.capacity);
  }
}
