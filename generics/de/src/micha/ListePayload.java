package de.src.micha;

public class ListePayload<E, P > extends Liste<E> {
  private P payload;
  public ListePayload(E ele, P pay) {
    super(ele);

    this.payload=pay;

  }
  public boolean vergleiche(ListePayload<E, P> lp1, ListePayload<E, P> lp2){
    return lp1.getElement().equals(lp2.getElement()) && lp1.getPayload().equals(lp2.getPayload());
  }

  public P getPayload(){
    return this.payload;
  }
}
