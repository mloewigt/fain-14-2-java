package analysemuster;

import java.util.*;
import java.util.Date;

/**
 * Created by root on 15.12.15.
 */
public class Bestellung {

    public static int klassenNummer=1;
    public String datum;
    public int rechnungsnummer;
    public List<Position> bestellung=new ArrayList<>();

    public Bestellung() {
        this.rechnungsnummer=klassenNummer;
        klassenNummer++;
        //this.datum=Calendar.getInstance().get(Calendar.DATE);
    }


    public void erfassePosition(Position p) {
        bestellung.add(p);
    }
    public void setDate(String s) {
        this.datum = s;
    }

    public void druckeDich() {
        System.out.println("Bestellung:");
        System.out.format("Datum: %8s \n Rechnungsnummer %8", datum, rechnungsnummer);
        bestellung.get(0).druckePosition();
    }
}
