package analysemuster;

/**
 * Created by root on 15.12.15.
 */
public class Position {

    int anzahl;
    int preis;
    Artikel art;


    public Position (Artikel a, int anzahl,int preis) {
        art=a;
        this.anzahl=anzahl;
        this.preis=preis;
    }

    public void druckePosition() {

        System.out.format("%4d, %4d, %6d", this.art.artikelNummer, this.anzahl, this.preis);
    }

}
