package analysemuster;

/**
 * Created by root on 15.12.15.
 */
public class MainApp {
    public static void main (String [] args) {
        Bestellung b = new Bestellung();

        Artikel a1 = new Artikel("Troxschraube M1");
        Artikel a2 = new Artikel("Sicherung 16A");
        Artikel a3 = new Artikel ("Bauholz, Vierkant, 2 m");

        Position p1= new Position(a1, 3, 12);
        Position p2= new Position(a2, 10, 2);
        Position p3= new Position(a3, 1, 5);

        b.erfassePosition(p1);
        b.erfassePosition(p2);
        b.erfassePosition(p3);
        b.setDate("15.11.2014");
        b.druckeDich();

    }
}
