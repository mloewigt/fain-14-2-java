package de.src.micha;

public class MainApp {
  public static void main(String [] args) {
    Box b = new Box();
    b.add(new String("Hallo"));
    System.out.format("Der Inhalt der Box ist: %s %n", b.getContent());

    Box b2 = new Box();
    b2.add(new Car());
    System.out.format("Der Inhalt der Box ist: %s %n", b2.getContent());

    ((Car)b2.getContent()).move(100);
    System.out.format("erneuter Aufruf: %s %n", b2.getContent());

    BoxGeneric <Car>bg = new BoxGeneric<Car>();
    bg.add(new Car());
    bg.getContent().move(500);

    System.out.format("Mit den Generics: %s %n", bg.getContent());
  }


}
