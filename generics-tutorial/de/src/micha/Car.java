package de.src.micha;


public class Car {
  private int mileage =0;

  public void move (int km) {
    this.mileage+=km;
  }

  @Override

  public String toString() {
    return String.format("Ich bin ein %s und habe %d km auf dem Tacho",
    this.getClass().getSimpleName(), this.mileage);
  }
}
