package observer;

import java.util.Observer;
import java.util.Observable;


/**
 * Created by root on 30.11.15.
 */
public class SpeedMonitor implements Observer {

            public static final int SPEED_TO_Alert=70;

            @Override
            public void update(Observable o, Object arg) {
                Speedometer speedo = (Speedometer) o;
                if(speedo.getCurrentSpeed()>SPEED_TO_Alert) {
                    System.out.println("Alarm -Sie fahren zu schnell "+speedo.getCurrentSpeed());
                } else {
                    System.out.println("Alles im grünen Bereich "+speedo.getCurrentSpeed());
                }


            }



}
