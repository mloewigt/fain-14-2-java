package observer;

/**
 * Created by root on 30.11.15.
 */
public class Intervallinspektor implements Inspektor {
    private int naechsteInspektion = 10000;

    public void inform(BeobachtetesVehicle v) {

        if(v.getMilage() >= naechsteInspektion) {

            System.out.println("Die Intervallinspektion wird durchgeführt");

            naechsteInspektion += 10000;
        }
    }
}