package observer;


import java.util.*;
import vehicle.*;
import engine.*;
/**
 * Created by root on 30.11.15.
 */
public class BeobachtetesVehicle extends AbstractVehicle {
    List<Inspektor> beobachter;

    public BeobachtetesVehicle(Engine e) {
        super (e);
        beobachter = new ArrayList<>();
    }
    public void addObserver(Inspektor o){

        beobachter.add(o);
        System.out.println(this.beobachter.toArray());
    }
    public void removeObserver(Inspektor o){
        beobachter.remove(o);
    }

    public void notifyMyObservers() {
        for(Inspektor o: beobachter) {
            o.inform(this);
        }
    }
    public int getMilage() {
        return milage;
    }
    @Override
    public void move(int km){
        super.move(km);
        this.notifyMyObservers();
    }
    @Override
    public int getPrice() {
        return 0;
    }

}
