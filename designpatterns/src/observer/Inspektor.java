package observer;

import vehicle.*;



/**
 * Created by root on 30.11.15.
 */
public interface Inspektor {
    public void inform(BeobachtetesVehicle v);
}
