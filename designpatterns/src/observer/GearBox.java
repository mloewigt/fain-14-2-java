package observer;

import java.util.Observable;
import java.util.Observer;
/**
 * Created by root on 30.11.15.
 */
public class GearBox implements Observer {
    @Override
    public void update(Observable o, Object arg) {
        Speedometer speedo = (Speedometer) o;

        if (speedo.getCurrentSpeed() <= 10) {
            System.out.println("Erster Gang ...");
        } else
            if (speedo.getCurrentSpeed() <= 20) {
                System.out.println("zweiter Gang ...");
            } else
                if (speedo.getCurrentSpeed() <= 30) {
                    System.out.println("dritter Gang..");
                } else
                    if (speedo.getCurrentSpeed() <= 50) {
                        System.out.println("vierter Gang ...");
                    } else
                        if (speedo.getCurrentSpeed() <= 70) {
                            System.out.println("fünfter Gang ...");
                        }
                    }
                }



