package observer;



/**
 * Created by root on 30.11.15.
 */
public class Erstinspektor implements Inspektor {

    private static final int KM = 5000;
    public void inform(BeobachtetesVehicle bv) {
        if(bv.getMilage()>=KM) {
            System.out.println("Erstinspektion fällig");
            bv.removeObserver(this);
        }
    }

}
