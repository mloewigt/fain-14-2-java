package observer;

import java.util.Observable;

/**
 * Created by root on 30.11.15.
 */
public class Speedometer extends Observable{
    private int currentSpeed;
    public Speedometer(){
        currentSpeed =0;
    }
    public void setCurrentSpeed(int speed) {
        currentSpeed=speed;
        setChanged();
        notifyObservers();

    }
    public int getCurrentSpeed() {
        return this.currentSpeed;
    }

}

