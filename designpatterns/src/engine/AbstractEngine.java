package engine;

/**
 * Created by root on 30.11.15.
 */


public abstract class AbstractEngine implements Engine {

    // gemeinsame Eigenschaften aller Motoren
    private int size;
    private boolean turbo;

    // die Eigenschaften dienen der Veranschaulichung des Bridge-Patterns
    private boolean running;
    protected int currentPower;

    public AbstractEngine(int size, boolean turbo) {

        this.size = size;
        this.turbo = turbo;

        this.running = false;
        this.currentPower = 0;
    }

    @Override
    public int getSize() {

        return this.size;
    }

    @Override
    public boolean isTurbo() {

        return this.turbo;
    }

    @Override
    public String toString() {

        return String.format("%s (%d)", this.getClass().getSimpleName(), this.size);
    }

    @Override
    public void start() {

        this.running = true;
        System.out.println("...starting engine");
    }

    @Override
    public void stop() {

        this.running = false;
        System.out.println("...stopping engine");
    }

    @Override
    public void increasePower() {

        // prüfen, ob der Motor gestartet wurde und wir den Maximalwert (z.B. 100%) schon erreicht haben
        if( this.running && (this.currentPower<100)) {

            this.currentPower += 1;

            System.out.println("Engine power increased to " + this.currentPower);
        }
    }

    @Override
    public void decreasePower() {

        // prüfen, ob der Motor läuft und wir den Minimalwert noch nicht erreicht haben
        if(this.running && (this.currentPower>0)) {

            this.currentPower -= 1;

            System.out.println("Engine power decreased to " + this.currentPower);
        }
    }

    @Override
    public void checkOil() {

        System.out.println("Ölstand geprüft");
    }

    @Override
    public void testRun() {

        this.start();
        this.increasePower();
        this.decreasePower();
        this.stop();
    }

    @Override
    public void getDocuments() {

        System.out.println("Erstelle Papiere");
    }

    @Override
    public void cleaning() {

        System.out.println("Motor gereinigt");
    }


}
