package Iterator;

import vehicle.*;
import engine.*;


import java.util.Arrays;
import java.util.Iterator;

/**
 * Created by root on 01.12.15.
 */
public class VanList implements Iterable <Vehicle> {
    private Vehicle[] vans = new Vehicle[3];

    public VanList() {
        vans[0]=new Pickup(new StandardEngine(3000));
        vans[1]=new BoxVan(new StandardEngine(2800));
        vans[2]=new Pickup(new TurboEngine(4000));
    }

    public Vehicle [] getList() {
        return vans;
    }


    @Override
    public Iterator<Vehicle> iterator() {
        return Arrays.asList(vans).listIterator();
    }
}
