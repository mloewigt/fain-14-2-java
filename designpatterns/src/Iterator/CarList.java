package Iterator;

import vehicle.*;
import engine.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by root on 01.12.15.
 */
public class CarList implements Iterable<Vehicle> {

    private List<Vehicle> cars;

    public CarList() {
        cars=new ArrayList<>();
        cars.add(new Saloon(new StandardEngine(1600)));
        cars.add(new Sport(new TurboEngine(2300)));
        cars.add(new Coupe(new TurboEngine(3000)));
        cars.add(new Saloon(new StandardEngine(1800)));
    }

    public List<Vehicle> getList() {
        return cars;
    }

    @Override
    public Iterator<Vehicle> iterator() {

        return cars.listIterator();
    }
}
