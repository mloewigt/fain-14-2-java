package Iterator;

import engine.*;
import vehicle.*;

import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.Queue.*;


/**
 * Created by root on 01.12.15.
 */
public class Wartung {

    public Queue<Vehicle> wartung;


    public Wartung() {
        wartung =new ArrayBlockingQueue<Vehicle>(10);

        wartung.offer(new Sport(new TurboEngine(2200)));
        wartung.offer(new BoxVan(new StandardEngine(2200)));
        wartung.offer(new Saloon(new StandardEngine(2200)));
        wartung.offer(new Pickup(new StandardEngine(2200)));
    }

    public void maintain() {
        System.out.println("Wartung für :"+wartung.poll()+" ist erledigt.");

    }

    public void showQueue() {
        System.out.println("noch in der warteschlange enthalten:");
        for (Vehicle v: wartung) {
            System.out.println(v);
        }
    }

    public void registerForMaintainance(Vehicle vh) {
        wartung.offer(vh);
    }

}
