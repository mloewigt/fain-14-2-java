import observer.GearBox;
import observer.SpeedMonitor;
import observer.Speedometer;

import engine.*;
import vehicle.*;
import observer.*;
import Iterator.*;

/**
 * Created by root on 30.11.15.
 */
public class MainApp {

    public static void main(String []args) {
        SpeedMonitor monitor = new SpeedMonitor();
        GearBox gearbox = new GearBox();
        Speedometer speedo = new Speedometer();
        speedo.addObserver(monitor);
        speedo.addObserver(gearbox);
        for(int v = 10; v<100; v+=10) {
            speedo.setCurrentSpeed(v);

        }

        // unser eigenes Beobachtermuster außerhalb der Standard-Java-Klassen

        BeobachtetesVehicle beobachtetesVehicle = new BeobachtetesVehicle(new StandardEngine(1300));

        // die Inspektoren erzeugen
        Erstinspektor ei = new Erstinspektor();
        Intervallinspektor ii = new Intervallinspektor();

        // Beobachter hinzufügen
        beobachtetesVehicle.addObserver(ei);
        beobachtetesVehicle.addObserver(ii);

        for(int count=0; count<20; count += 1) {

            // das Fahrzeug 1000 km fahren lassen
            beobachtetesVehicle.move(1000);
        }

        //Iterator-Pattern

        CarList carlist = new CarList();
        VanList vanlist = new VanList();

        PrintBrochure.printIterator(carlist.iterator());
        PrintBrochure.printIterator(vanlist.iterator());

        Wartung w = new Wartung();
        w.maintain();
        w.showQueue();
        w.registerForMaintainance(new Coupe(new TurboEngine(2200)));
        w.showQueue();


    }
}


