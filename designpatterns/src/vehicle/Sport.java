package vehicle;

import engine.*;

/**
 * Created by root on 01.12.15.
 */


public class Sport extends AbstractCar {

    // Konstruktor
    public Sport(Engine engine) {

        this(engine, Vehicle.Colour.UNPAINTED);
    }

    // Konstruktor
    public Sport(Engine engine, Vehicle.Colour colour) {

        super(engine, colour);
    }

    @Override
    public int getPrice() {

        return 8000;
    }

    @Override
    public int getDailyRate() {

        return 79;
    }
}