package vehicle;

import engine.*;

/**
 * Created by root on 01.12.15.
 */


public class Saloon extends AbstractCar {

        // Konstruktor
        public Saloon(Engine engine) {

            this(engine, Vehicle.Colour.UNPAINTED);
        }

        // Konstruktor
        public Saloon(Engine engine, Vehicle.Colour colour) {

            super(engine, colour);
        }

        @Override
        public int getPrice() {

            return 6000;
        }

        @Override
        public int getDailyRate() {

            return 49;
        }
    }
