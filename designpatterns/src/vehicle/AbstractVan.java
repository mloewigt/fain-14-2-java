package vehicle;

import engine.*;

/**
 * Created by root on 01.12.15.
 */



public abstract class AbstractVan extends AbstractVehicle {

    // Konstruktor
    public AbstractVan(Engine engine) {

        this(engine, Vehicle.Colour.UNPAINTED);
    }

    // Konstruktor
    public AbstractVan(Engine engine, Vehicle.Colour colour) {

        super(engine, colour);
    }
}
