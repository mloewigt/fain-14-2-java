package vehicle;

import engine.*;
/**
 * Created by root on 01.12.15.
 */


public class BoxVan extends AbstractVan {

        // Konstruktor
        public BoxVan(Engine engine) {

            this(engine, Vehicle.Colour.UNPAINTED);
        }

        // Konstruktor
        public BoxVan(Engine engine, Vehicle.Colour colour) {

            super(engine, colour);
        }

        @Override
        public int getPrice() {

            return 10000;
        }

        @Override
        public int getDailyRate() {

            return 89;
        }
    }
