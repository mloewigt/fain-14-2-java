package vehicle;

import engine.*;

/**
 * Created by root on 01.12.15.
 */
public class Coupe extends AbstractCar {

    // Konstruktor
    public Coupe(Engine engine) {

        this(engine, Vehicle.Colour.UNPAINTED);
    }

    // Konstruktor
    public Coupe(Engine engine, Vehicle.Colour colour) {

        super(engine, colour);
    }

    @Override
    public int getPrice() {

        return 7000;
    }

    @Override
    public int getDailyRate() {

        return 59;
    }
}