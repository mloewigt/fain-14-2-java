package vehicle;

import engine.*;
/**
 * Created by root on 01.12.15.
 */


public abstract class AbstractCar extends AbstractVehicle {

    // Konstruktor
    public AbstractCar(Engine engine) {

        this(engine, Vehicle.Colour.UNPAINTED);
    }

    // Konstruktor
    public AbstractCar(Engine engine, Vehicle.Colour colour) {

        super(engine, colour);
    }
}