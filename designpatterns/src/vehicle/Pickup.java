package vehicle;

import engine.*;
import vehicle.*;

/**
 * Created by root on 01.12.15.
 */


public class Pickup extends AbstractVan {

        // Konstruktor
        public Pickup(Engine engine) {

            this(engine, Vehicle.Colour.UNPAINTED);
        }

        // Konstruktor
        public Pickup(Engine engine, Vehicle.Colour colour) {

            super(engine, colour);
        }

        @Override
        public int getPrice() {

            return 9000;
        }

        @Override
        public int getDailyRate() {

            return 79;
        }
    }

