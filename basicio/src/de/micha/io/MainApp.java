package src.de.micha.io;

import java.io.IOException;

public class MainApp {

  public static void main(String [] args) throws IOException {

        CopyByteStream cbs = new CopyByteStream();
        cbs.copy(args[0], args[1]);
        //


        CopyCharacterStream ccs = new CopyCharacterStream();
        ccs.copy(args[0], args[2]);

        CopyLineStream cls= new CopyLineStream();
        cls.copy(args[0], args[3]);

        ZeilenScanner cs = new ZeilenScanner();
        cs.copy(args[0]);
  }


}
