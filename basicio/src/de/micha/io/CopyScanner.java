package src.de.micha.io;

import java.io.FileReader;
import java.io.BufferedReader;

import java.io.IOException;
import java.io.FileNotFoundException;

import java.util.Scanner;

public class CopyScanner {
      public void copy(String input){
        Scanner scan =null;

        try {

          scan = new Scanner (new BufferedReader(new FileReader(input)));
          while(scan.hasNext()) {
            //wir holen uns dad nächste Token aus dem Stream
            System.out.println(scan.next());

          }
        }
        catch(FileNotFoundException e){
          System.out.println(e);
        }
        finally{
          if(scan != null) {
            // der Aufruf close() wird dan das injizierte BufferedRader-Objekt delegiert
            scan.close();
          }
        }

      }

}
