package src.de.micha.io;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;


public class CopyByteStream {
  public void copy(String input, String output) throws IOException {
    FileInputStream in = null;
    FileOutputStream out = null;

    try {
      //ggfs. wird eine FileNoFoundException geworfen
      in =  new FileInputStream(input);

      out = new FileOutputStream(output);
      int c;
      while((c=in.read())!=-1){
        // wir schreiben das eingelesene Byte in den FileOutputStream
        out.write(c);
      }
    }
    catch (FileNotFoundException e){
        System.out.println(e);
    }
    finally {
      //close() wird ggfs. eine IOException
        if (in!=null){
          in.close();
        }
        if(out!=null){
          out.close();
        }
    }
  }
}
