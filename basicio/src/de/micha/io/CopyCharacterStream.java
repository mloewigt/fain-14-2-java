package src.de.micha.io;

import java.io.FileReader;
import java.io.FileWriter;

import java.io.IOException;
import java.io.FileNotFoundException;

public class CopyCharacterStream {
      public void copy(String input, String output) throws IOException {

            FileReader in = null;
            FileWriter out = null;




    try {
      // die Erzeugung de Objekte in einen try-catch-Block einfassen
      //sonst Ermahnung vom Compiler auffangen der checked Exception
      in = new FileReader(input);
      out = new FileWriter(output);

      int c;
      while((c=in.read())!=-1) {
        //read() liefert dieses mal einen 16 Bit-Wert
        out.write(c);
      }
    }
    catch(FileNotFoundException e) {
      System.out.println(e);
    }
    finally {
      if (in!=null){
        in.close();
      }
      if(out!=null){
        out.close();
      }
    }
  }
}
