package de.src.aufgabe;


public abstract class AVehicle implements Vehicle {
  public static int id=1;

  protected int autonummer;
  protected int tacho;
  protected String farbe;
  protected int leistung;
  protected String hersteller;
  protected Status status;

  public AVehicle(){
    this.autonummer=id++;
    this.tacho=0;
    this.farbe="schwarz";
    this.leistung= 150;
    this.hersteller="benz";
    this.status =EINSATZBEREIT;

  }

  public AVehicle(int km, String f, int l, String prod, Status s){
    this.autonummer=id++;
    this.tacho=km;
    this.farbe=f;
    this.leistung= l;
    this.hersteller=prod;
    this.status =s;

  }

  public String getStatus(){
    if (this.status == EINSATZBEREIT)
      return "einsatzbereit";
    else
        return "in Wartung";
  }
  @Override
  public void move(int km){
    this.tacho +=km;
  }

  @Override
  public String toString () {
    return String.format("ich bin %d %n", this.autonummer)
  }



}
