package de.src.micha.collections;

import java.util.*;

public class CarSort {

	protected String farbe;
	protected Integer kmStand;
	protected Integer motorleistung;


	public CarSort() {
		this.farbe="rot";
		this.kmStand=0;
		this.motorleistung=0;
	}

	public CarSort(String f, int k, int m){
		this.farbe=f;
		this.kmStand=k;
		this.motorleistung=m;
	}


	@Override
		public String toString() {
			return String.format("[%10s, %6d, %4d]", this.farbe, this.kmStand, this.motorleistung);
		}

	// anonyme Klassen
	protected static Comparator<CarSort> COMP_KM = new Comparator<CarSort>(){
		public int compare(CarSort c1, CarSort c2) {
			return c1.kmStand.compareTo(c2.kmStand);
		}
	};

  /*
  protected static Compartor<CarSort> COMP_FARBE =new Comparator <CarSort> () {
    public int compare(CarSort c1,CarSort c2){
      return c1.farbe.compareTo(c2.farbe);
    }
  };
  */
}
