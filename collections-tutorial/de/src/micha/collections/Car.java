package de.src.micha.collections;

public class Car implements Comparable<Car> {

  private static int id=1;

  private int nummer;
  private int kilometerstand;
  private String farbe;
  private int motorleistung;
  private String hersteller;

  //Aufgabe: Vervollständigen Sie die Klasse! Implementieren Sie equals und hashcode

  public Car( int km, String f, int ps, String hersteller) {
    this.nummer=id++;
    this.kilometerstand=km;
    this.farbe=f;
    this.motorleistung=ps;
    this.hersteller=hersteller;

  }


  public boolean equals(Object o){
     return this.nummer==((Car)o).nummer;

  }

  public int hashCode() {
    int hash =1;
    hash = 17*hash +this.nummer;

    return hash;

  }

  @Override
  public int compareTo(Car c){

      return this.nummer-c.nummer;
  }




}
