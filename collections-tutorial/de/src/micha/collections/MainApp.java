package de.src.micha.collections;

//import java.util.List;
//import java.util.ArrayList;
// oder alternativ

import java.util.*;
import java.util.concurrent.*;
import java.util.ArrayDeque.*;


public class MainApp {
  public static void main(String [] args) {
  List<String> list = new ArrayList <String>();
  list.add("hallo");
  list.add(new String("Welt!"));
  list.add("eins");
  list.add("eins");


  List<String> andereListe = new ArrayList <String>(list);

  List<String> nochEineListe = new ArrayList <String>(1000);

  System.out.format("list [%d], andere Liste [%d], nochEineListe[%d]%n", list.size(), andereListe.size(), nochEineListe.size());

  andereListe.add("wir");
  andereListe.add("brauchen");
  andereListe.add("eine Main-Methode");
  System.out.format("list [%d], andere Liste [%d], nochEineListe[%d]%n", list.size(), andereListe.size(), nochEineListe.size());
  System.out.format("die Liste ist in der andereListe enthalten: %b %n", andereListe.containsAll( list));

  for (String s: andereListe){
    System.out.println(s);
  }
  String tmp = andereListe.remove(2);
  System.out.println("___________________________________");
  for (String s: andereListe){
    System.out.println(s);
  }
  System.out.println("Entfernt wurde "+tmp);

  Car a1 = new Car( 1000, "blau", 150, "daimler");
  Car a2 =new Car( 1000, "blau", 150, "daimler");
  Car a3 =new Car( 0, "rot", 65, "fiat");

  if(a1.equals(a2))
    System.out.println("die karren sind gleich");
  else
    System.out.println("sie unterscheiden sich");
    int code;
    code = a1.hashCode();
    System.out.println("der hashcode: " +code);
    code = a2.hashCode();
    System.out.println("der hashcode: " +code);

    Set<Car> carSet=new TreeSet<Car>();
    carSet.add(a1);
    carSet.add(a2);
    carSet.add(a3);
    System.out.format("carSet [%d] %n", carSet.size());

    /**
    * Interface List
    */

    List<Integer> intListe = new ArrayList<Integer>();

    //Elemente hinzufügen (auch Duplikate)

    intListe.add(10);
    intListe.add(10);
    intListe.add(10);
    intListe.add(10);
    intListe.add(20);
    intListe.add(20);
    intListe.add(20);
    intListe.add(30);
    intListe.add(30);
    intListe.add(30);
    intListe.add(30);
    System.out.println(intListe);

    System.out.format("Das Objekt 10 befindet sich an Stelle %d %n", intListe.indexOf(10));
    System.out.format("Das Objekt 10 kommt zum letzten Mal an der Stelle %d vor. %n", intListe.lastIndexOf(10));

    System.out.println("das ist eine Subliste von intListe: "+intListe.subList(4,8));

    /**
    *Interface Queue
    */

      Queue<Integer> queue = new LinkedList <Integer>();

      for(int i=10; i>=0; i -=1){
        queue.add(i);
      }
      System.out.println(queue);
      System.out.println("Das erste Element der queue: "+queue.peek());

      while(!queue.isEmpty()){
        System.out.println(queue.remove());
        System.out.println("Das erste Element der queue: "+queue.peek());
      }

      Queue <Integer> arrayQueue =new ArrayBlockingQueue<Integer>(10);

      for (int i =0; i<10;i+=1) {
        System.out.format("%s: %b %n", arrayQueue, arrayQueue.add(i));
      }
      //System.out.format("%s: %1$b %n", arrayQueue, arrayQueue.offer(100));
      System.out.format("%s: %b %n", arrayQueue, arrayQueue.offer(101));

      /**
      * das Interface Deque
      *
      */

      Deque<Integer> deque = new ArrayDeque<Integer>(10);

      System.out.format("Deque: %s mit  %d Anzhal Elementen%n",deque, deque.size());

      deque.offerFirst(10);
      deque.offerFirst(20);
      deque.offerLast(30);
      deque.offerLast(40);

      System.out.format("Deque: %s mit  %d Anzhal Elementen%n",deque, deque.size());

      deque.addFirst(50);
      deque.addFirst(60);
      deque.addFirst(70);
      deque.addFirst(80);
      deque.addLast(90);
      deque.addLast(100);

      deque.addLast(110);
      System.out.format("Deque: %s mit  %d Anzhal Elementen%n",deque, deque.size());


      /**
      * das Interface Map
      *
      */

      Map<Integer, String> map = new HashMap<>();

      map.put(1, "Hallo");
      map.put(2, "Welt!");
      map.put(3, "Wir reden hier über das");
      map.put(3, "mal schauen, ob das geht?");

      System.out.println(map);

      Set <Integer> keys=map.keySet();
      Collection<String> values=map.values();

      System.out.println("keys ist ein: "+keys.getClass());
      System.out.println("Values ist ein: "+values.getClass());


      System.out.println("Schüssel: "+keys);
      System.out.println("werte: "+values);

      System.out.format("Der Schlüssel 3 ist in der Map enthalten: %b%n", map.containsKey(3));
      System.out.format("Der Schlüssel 4 ist in der Map enthalten: %b%n", map.containsKey(4));

      System.out.format("Der Wert \"Hallo\"  ist in der Map enthalten: %b%n", map.containsValue("Hallo"));
      System.out.format("Der Wert \"muttiheft\"  ist in der Map enthalten: %b%n", map.containsValue("Muttiheft"));


      /**
      * Arbeiten mit Comparator
      *
      */

      List<CarSort> lsc = new ArrayList<CarSort>();

      lsc.add( new CarSort("rot", 1000, 120));
      lsc.add( new CarSort("blau", 2000, 100));
      lsc.add( new CarSort("silber", 30000, 220));
      lsc.add( new CarSort("grau", 4000, 80));
      lsc.add( new CarSort("lila", 100000, 75));
      lsc.add( new CarSort("braun", 10, 320));

      System.out.println(lsc);

      //wir lassen mit dem Comparator sortieren

      //Collections.sort(lsc, new CarSortCmpFarbe());
      System.out.println("Sortiert nach Farbe");
      System.out.println(lsc);
      System.out.println("neuer Versuch");
      //Collections.sort(lsc, CarSort.C0MP_KM);


      System.out.println(lsc);
      /*
      Collections.sort(lsc, new CarSortCmpMotor());
      System.out.println("Sortiert nach Motorleistung");
      System.out.println(lsc);
      Collections.sort(lsc, new CarSortCmpMileage());
      System.out.println("Sortiert nach kmStand");
      System.out.println(lsc);
      */

      List <Integer> integerListe = new LinkedList<Integer>();

      integerListe.add(-100);
      integerListe.add(0);
      integerListe.add(75);
      integerListe.add(-12);
      integerListe.add(20);

      System.out.println("unsortiert: "+integerListe);
      Collections.sort(integerListe, null);
      System.out.println("sortiert: "+integerListe);
      Collections.reverse(integerListe);
      System.out.println("umgekehrte Reihenfolge: "+integerListe);
      Collections.shuffle(integerListe);
      System.out.println("gemischt: "+integerListe);
      Random rand =new Random();
      Collections.shuffle(integerListe, rand);
      System.out.println("gemischt mit Random: "+integerListe);
      Collections.shuffle(integerListe, rand);
      System.out.println("gemischt mit Random: "+integerListe);
      Collections.rotate(integerListe, 2);
      System.out.println("rotiert: "+integerListe);

      System.out.format("max %d, min %d %n", Collections.max(integerListe), Collections.min(integerListe));
      Collections.sort(integerListe);
      System.out.format("Die 20 steht an der Index  %d %n", Collections.binarySearch(integerListe, 20));
      Collections.fill(integerListe, 1000);
      System.out.println(integerListe);


    }
}
