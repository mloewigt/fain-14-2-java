package de.src.micha.collections;

import java.util.*;

 public class CarSortCmpMotor implements Comparator<CarSort> {

      @Override
      public int compare(CarSort c1, CarSort c2) {
          Integer a = new Integer(c1.motorleistung);
          Integer b = new Integer(c2.motorleistung);
            return a.compareTo(b);
      }

 }
