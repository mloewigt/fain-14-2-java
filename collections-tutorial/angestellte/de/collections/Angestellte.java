package de.collections;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.*;
import java.util.Date;
import java.text.SimpleDateFormat;


public class Angestellte {

    protected String name;
    protected String vorname;
    protected Float gehalt;
    protected Calendar hiredate;
    protected Calendar firedate;


    public Angestellte () {
        this.name="ber";
        this.vorname="hajr";
        this.gehalt= new Float( 10235.23);
        this.hiredate=new GregorianCalendar(2015,2,2);
        this.firedate= new GregorianCalendar(2017,1,1);


    }
    public Angestellte (String n, String v, double g, int hd, int hm, int hy) {
        this.name=n;
        this.vorname=v;
        this.gehalt=  new Float( (float)g);
        this.hiredate=new GregorianCalendar(hy,hm,hd);

        this.firedate=null;

    }

    @Override
    public String toString(){
      String status;
      if (firedate==null) {
        status = "immer noch dabei";
      } else {
          status = String.format ("gefeuert am %8TD", this.firedate);
      }
      return String.format("Name: %6s %10s Gehalt: %8.2f von %5TF %12s", this.name, this.vorname, this.gehalt, this.hiredate, status);
    }

    public String schreibGehalt(){

      return String.format("Name: %6s %10s Gehalt: %8.2f", this.name, this.vorname, this.gehalt);
    }
    public String schreibDatum(){
      SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
      StringBuilder sb = new StringBuilder();
      //return String.format("Name: %6s %10s Einstelldatum: %TF", this.name, this.vorname, this.hiredate);
      //return String.format("Name: %10s %10d %d %s", this.name, this.vorname, dateFormat.format(this.hiredate.getTime()));
      sb.append(String.format("Name: %6s %10s %14s", this.name, this.vorname, dateFormat.format(this.hiredate.getTime())));
      return sb.toString();
    }
    //%1$tm %1$te,%1$tY


    protected static Comparator <Angestellte> GEHALT= new Comparator<Angestellte>(){
      public int compare(Angestellte a, Angestellte b){
        return a.gehalt.compareTo(b.gehalt);
      }
    };

    protected static Comparator <Angestellte> EINSTELLDATUM= new Comparator<Angestellte>(){
      public int compare(Angestellte a, Angestellte b){
        return a.hiredate.compareTo(b.hiredate);
      }
    };






}
