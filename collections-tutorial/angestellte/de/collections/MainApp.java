package de.collections;

import java.util.*;
import java.util.concurrent.*;


public class MainApp {

    public static void main(String [] args){
      List<Angestellte> staff = new ArrayList<Angestellte>();



      staff.add( new Angestellte());
      //public Angestellte (String n, String v, float g, int hd, int hm, int hy) {
      staff.add(new Angestellte("carl", "friedrich", 50000.64, 11, 10, 2015 ));
      staff.add(new Angestellte("mira", "mondroe", 110.56, 8, 6, 2010 ));

      System.out.println(staff);
      System.out.println("Sortiert nach Gehalt");
      Collections.sort(staff, Angestellte.GEHALT);

      for(Iterator <Angestellte> istaff = staff.iterator(); istaff.hasNext();)
      System.out.println(istaff.next().schreibGehalt());
      //System.out.println((staff.get(0)).schreibGehalt());
      //System.out.println((staff.get(1)).schreibGehalt());
      System.out.println("Sortiert nach Betriebszugehörigkeit");
      Collections.sort(staff, Angestellte.EINSTELLDATUM);
      for(Iterator <Angestellte> istaff = staff.iterator(); istaff.hasNext();)
      System.out.println(istaff.next().schreibDatum());
    }

}
